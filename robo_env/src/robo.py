#!/usr/bin/env python

import numpy as np

import rospy
import math
import time

import json

from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from sensor_msgs.msg import LaserScan
from location_monitor.msg import LandmarkDistance
from location_monitor.msg import LaserScanDistance
from location_monitor.msg import LaserScanSectorsDistance

from robo_env.msg import MoveAction

from rl_msgs.msg import RLStateReward 
from rl_msgs.msg import RLAction 
from rl_msgs.msg import RLEnvDescription
from rl_msgs.msg import RLEnvSeedExperience
from rl_msgs.msg import RLExperimentInfo

from std_srvs.srv import Empty, EmptyResponse


# exp object
class Experience: 
	s 	= [0, 0, 0, 0, 0]
	next 	= [0, 0, 0, 0, 0]
	act 	= 0
	reward  = 0
	terminal = False

# robo environment
class Robo:
	actNum  = 0
	s 	= [0, 0, 0, 0, 0]
	sOld 	= [0, 0, 0, 0, 0]
	minFeat = []
	maxFeat = []

	# experience
	expData = []
	expFileName = "/home/dsg/catkin_ws/src/robo_env/src/exp.json"
	expFile = ''
	rlActionCounter = 0

	def __init__(self):
		self.reset()
		self.prepareExpFile()

	def apply(self, action):
		self.actNum += 1

		'''
		0   = 1
		1,2 = 0.1
		3,4 = 0.25
		5,6 = 0.5
		7,8 = 1
		'''

		ma = MoveAction()
		ma.move_action = action
		outMoveAction.publish(ma)

		# import to have here time delay. env should be synchronized with sensors and movements
		time.sleep(0.5)
		
		return self.reward()

	# reset coordinates, velocity, angle of turtlebot
	def reset(self):
		rospy.wait_for_service('/gazebo/reset_world')
		resetWorldSrv = rospy.ServiceProxy('/gazebo/reset_world', Empty)
		resetWorldSrv()

		#rospy.wait_for_service('/gazebo/reset_simulation')
		#resetSimulationSrv = rospy.ServiceProxy('/gazebo/reset_simulation', Empty)
		#resetSimulationSrv()

		self.actNum = 0
		self.s = [0, 0, 0, 0, 0]

	def terminal(self):
		'''
			right sector
				a) too close = 0 
				c) too far   = 4
			right frontal
				a) too close = 0 vremeno ubiraem
			front sector
				a) too close = 0
			left sector
				a) too close = 0
		or self.s[1] == 0
		'''
		return self.s[0] == 0 or self.s[2] == 0 or self.s[3] == 0 or (self.s[0] == 4 and self.s[4] == 0)

	def getNumActions(self):
		# 0=0, 1=-10, 2=10, 3=-20, 4=-20, 5=-40, 6=40
	    	return 7

	def getMinMaxFeatures(self):

		for i in range(0, len(self.s) ):
			self.minFeat.append(0.0) #
			self.maxFeat.append(4.0) # maxFeat[0] = too close, close, medium, far, very far
		
		'''
		correcting some features, for example slope has min=0 and max=1, so right this feature was corrected
		'''
		self.maxFeat[1] = 1 # correction right front sector   0 - close medium, 1 far
		self.maxFeat[2] = 3 # correction to front sector      0 - too close, 1-close, 2-medium, 3-far
		self.maxFeat[3] = 1 # correction to left sector	      0 - close, 1 far
		self.maxFeat[4] = 3 # correction to slope, max is 3 , 0 - not recognized, 1 - parallel, 2- away from wall, 3 - torward the wall

		return [self.minFeat, self.maxFeat]

	# get current state object
	def sensation(self):
		return self.s
	
	# set state object [usually for test]
	def setSensation(self, newS): 
		if len(s) <> len(newS): 
			print "Error in sensation sizes"
		
		#for x range len(newS) <--- doest work but i don't need it right now
		#s[x] = newS[x]

	def getMinMaxReward(self):
  		return [-1, 1]

	def reward(self):
		if self.terminal() == True:
			return -1
		#elif self.s[0] == 1:
			#return 0.5
		elif self.s[0] == 2:
			return 1
		else:
			return 0

	def isEpisodic(self):
		return True

	def obstacleDistanceCb(self, msg):
		self.s[0] = msg.right_sec_distance
		self.s[1] = msg.right_front_sec_distance
		self.s[2] = msg.front_sec_distance
		self.s[3] = msg.left_sec_distance
		self.s[4] = msg.slope

	def getSeedings(self):
		seeds = []
		try:
			self.readExp()

			# read exp from file , parse to json objects
			for i in xrange(0, len( self.expData) ):
				s0 = self.expData[i]['s0']  # r
				s1 = self.expData[i]['s1']  # rf
				s2 = self.expData[i]['s2']  # f
				s3 = self.expData[i]['s3']  # left
				s4 = self.expData[i]['s4']  # slope

				a  = self.expData[i]['a']   # action

				n0 = self.expData[i]['n0']  # next state
				n1 = self.expData[i]['n1']
				n2 = self.expData[i]['n2']
				n3 = self.expData[i]['n3']
				n4 = self.expData[i]['n4']

				reward = self.expData[i]['r']
				terminal = self.expData[i]['t']

				seeds.append(self.getExp( s0, s1, s2, s3, s4, a, False, n0, n1, n2, n3, n4, reward, terminal) )
		except ValueError:
			print("Oops!  {}").format(ValueError)

		return seeds

	def getExp(self, s0, s1, s2, s3, s4, a, simulate, n0, n1, n2, n3, n4, reward, terminal):
		# init
		ep = Experience()		
		ep.s 	= [0, 0, 0, 0, 0]
		ep.next = [0, 0, 0, 0, 0]
		
		self.s[0] = s0
		self.s[1] = s1
		self.s[2] = s2
		self.s[3] = s3
		self.s[4] = s4

		ep.act		= a
		ep.s 	 	= self.sensation()

		if simulate == True:
			ep.reward 	= self.apply(ep.act)
			ep.terminal 	= self.terminal()
			ep.next 	= self.sensation()

			if ep.terminal == True:
				self.reset()

		else:
			ep.reward = reward
			ep.terminal = terminal
			ep.next = [n0, n1, n2, n3, n4]

		return ep


	def prepareExpFile(self):
		self.expFile = open(self.expFileName, 'r+')

	def saveExp(self):
		self.expFile.seek(0)
		self.expFile.truncate()
		json.dump(self.expData, self.expFile)

	def readExp(self):
		self.expData = json.load(self.expFile)

'''
process action from the agent
'''
def processAction(actionIn):
	sr = RLStateReward()

	# process action from the agent, affecting the environment
	sr.reward 	= e.apply(actionIn.action)
	sr.state 	= e.sensation() # get current state
	sr.terminal	= e.terminal()  
	
	exp = {
		"s0": e.sOld[0], 
		"s1": e.sOld[1], 
		"s2": e.sOld[2], 
		"s3": e.sOld[3], 
		"s4": e.sOld[4],
		"a": actionIn.action,
		"n0": sr.state[0],
		"n1": sr.state[1],
		"n2": sr.state[2],
		"n3": sr.state[3],
		"n4": sr.state[4],
		"r": sr.reward,
		"t": sr.terminal  
	}

	e.expData.append( exp )

	e.sOld = sr.state  #sOld is updated only before saving exp in history file , sOld contains old state, e.s = new state

	if e.rlActionCounter == 50:
		e.saveExp()
		e.rlActionCounter = 0
	else:
		e.rlActionCounter += 1

	# publish the state-reward message
	print "Got action: {} at state: {}, {}, {}, {}, {}, reward: {}".format(actionIn.action, sr.state[0], sr.state[1], sr.state[2], sr.state[3], sr.state[4], sr.reward)
	
	outEnvSr.publish(sr)

''' 
Process end-of-episode reward info. 
Mostly to start new episode. 
'''
def processEpisodeInfo(infoIn):
	# start new episode if terminal
	print "Episode {}  terminated with reward: {}, start new episode ".format(infoIn.episode_number, infoIn.episode_reward)

	e.reset()
	sr = RLStateReward()
	sr.terminal 	= False
	sr.reward 	= 0
	e.sOld 		= e.sensation()
	sr.state 	= e.sensation()
	outEnvSr.publish(sr)

def sendRlEnvDescription():
	desc = RLEnvDescription()
	desc.title = 'Robo'
	desc.num_actions = e.getNumActions()
	desc.episodic = e.isEpisodic()

	# set states
	minMaxFeatures = e.getMinMaxFeatures()
	desc.num_states = len(minMaxFeatures[0])
	desc.min_state_range = minMaxFeatures[0]
	desc.max_state_range = minMaxFeatures[1]

	desc.stochastic = False;

	# set rewards and actions
	minMaxReward 		= e.getMinMaxReward()
	desc.max_reward 	= minMaxReward[1]
	desc.reward_range 	= minMaxReward[1] - minMaxReward[0]
	
	# publish environment description
	outEnvDesc.publish(desc)
	print "\nEnvironment Description was sent"
	#print desc

	# import to have here delay
	time.sleep(1)

        #send experiences
	seeds = e.getSeedings()

	for i in xrange(0, len(seeds) ):
		seed = RLEnvSeedExperience()
		seed.from_state = seeds[i].s
		seed.to_state   = seeds[i].next
		seed.action     = seeds[i].act
		seed.reward     = seeds[i].reward
		seed.terminal   = seeds[i].terminal
		outSeed.publish(seed)
	print "\nExp was sent"
	
	# now send first state message
	
	sr = RLStateReward()
	sr.terminal 	= False
	sr.reward 	= 0
	e.sOld 		= e.sensation()
	sr.state 	= e.sensation()
	outEnvSr.publish(sr)

	print "\nFirst State Message was sent"
	print sr
	
# Main function.
if __name__ == '__main__':
	# Initialize the node and name it. //, anonymous = True
	rospy.init_node('robo_node')
	
	e = Robo()

	# Set up Publishers
	outEnvDesc 	= rospy.Publisher('rl_env/rl_env_description', RLEnvDescription, queue_size=1, latch=True)
	outEnvSr   	= rospy.Publisher("rl_env/rl_state_reward",RLStateReward, queue_size=1)
	outSeed    	= rospy.Publisher("rl_env/rl_seed",RLEnvSeedExperience, queue_size=20)
	outMoveAction 	= rospy.Publisher('/robo/move_action', MoveAction, queue_size=1)	

	# Set up Subscribers
	rlAction   = rospy.Subscriber('rl_agent/rl_action', RLAction, processAction, queue_size=1)
	rlExpInfo  = rospy.Subscriber('rl_agent/rl_experiment_info', RLExperimentInfo, processEpisodeInfo)

	rospy.Subscriber('/lm_obstacle_distance', LaserScanSectorsDistance, e.obstacleDistanceCb, queue_size=1)

	# Send environment Description to rl_agent and right after that first state
	sendRlEnvDescription()

	# Go to the main loop.
	rospy.spin()
