#!/usr/bin/env python

'''
Author: Dmitry Sagoyan
Date: 11.01.2017

Description:
Ros-Node represents RL Agent. For the communication between the agent and the environment is used 'rl_textplore' package.

Libraries:

1. rl_textplore:
The stacks directory contains a ROS stack for reinforcement learning (RL). The
code in this repository provides agents, environments, and multiple ways for
them to communicate (through ROS messages, or by including the agent and 
environment libraries). There are 5 packages in the repository:

  rl_common:     Some files that are common to both agents and environments.
  rl_msgs:       Definitions of ROS messages for agents and envs to communicate.
  rl_agent:      A library of some RL agents including Q-Learning and TEXPLORE.
  rl_env:        A library of some RL environments such as Taxi and Fuel World.
  rl_experiment: Code to run some RL experiments without ROS message passing.

http://wiki.ros.org/rl-texplore-ros-pkg
'''

# Import required Python code.
import numpy as np
import roslib

import rospy
import math
import time
from collections import deque

from geometry_msgs.msg import Twist
from nav_msgs.msg import Odometry
from sensor_msgs.msg import LaserScan
from location_monitor.msg import LandmarkDistance
from location_monitor.msg import LaserScanDistance
from location_monitor.msg import LaserScanSectorsDistance

from robo_env.msg import MoveAction

from rl_msgs.msg import RLStateReward 
from rl_msgs.msg import RLAction 
from rl_msgs.msg import RLEnvDescription
from rl_msgs.msg import RLEnvSeedExperience
from rl_msgs.msg import RLExperimentInfo

from std_srvs.srv import Empty, EmptyResponse

roslib.load_manifest('robo_agent')

import sys
import os
sys.path.append(os.path.abspath('/home/dim/catkin_ws/src/robo_agent/src/rl'))
from pg_reinforce import PolicyGradientREINFORCE

import tensorflow as tf

class RoboAgent:
	'''
	The RLExperimentInfo message contains the sum of rewards each episode published by the agent.
	We can plot this value to examine the rewards per episode the agent is receiving. 
	To plot the episode_reward from this message: rxplot /rl_agent/rl_experiment_info/episode_reward
	'''
	info = RLExperimentInfo()

	'''
	If firstAction == True then first_action()
	else next_action()
	'''
	firstAction = True

	episodeTimesteps = 0

	episodeHistory = deque(maxlen=100)

	def __init__(self):
		print 'Agent: RoboAgent'
		#self.sess      = tf.Session()
		#self.optimizer = tf.train.RMSPropOptimizer(learning_rate=0.0001, decay=0.9)
		#self.writer    = tf.train.SummaryWriter("/tmp/{}-experiment-1".format('test'))

		# Set up publishers
		self.outRLAction = rospy.Publisher('rl_agent/rl_action', RLAction, queue_size=1, latch=False)
		self.outExpInfo  = rospy.Publisher('rl_agent/rl_experiment_info', RLExperimentInfo, queue_size=1, latch=False)

		# Set up subscribers
		self.rlDescription 	=  rospy.Subscriber("rl_env/rl_env_description", RLEnvDescription, self.processEnvDescription, queue_size=1)
		self.rlState		=  rospy.Subscriber("rl_env/rl_state_reward", RLStateReward, self.processState, queue_size=1)
		self.rlSeed 		=  rospy.Subscriber("rl_env/rl_seed", RLEnvSeedExperience, self.processSeed, queue_size=20 )
		
	def first_action(self, s, r):
		#print "First: State {} Reward {}".format(s,r)
		return self.getActionAndCollect(np.array(s), r)

	def next_action(self, s, r):
		#print "Next: State {} Reward {}".format(s,r)
		return self.getActionAndCollect(np.array(s), r)		

	def last_action(self, s, r):
		#print "Last: State {} Reward {}".format(s,r)
		return self.getActionAndCollect(np.array(s), r)

	def savePolicy(self, filename):
		return 1

	def loadPolicy(self, filename):
		return 1

	def printState(self):
		return 1

	def getActionAndCollect(self, state, reward):
		action = self.pgReinforce.sampleAction(state[np.newaxis,:])

	        self.pgReinforce.storeRollout(state, action, reward)

		return action

	# common robo_agent functionality

	'''
	this is a message from the environment with the agent's new state and reward received on this time step.

	state: [0.0, 0.0, 0.0, 0.0, 0.0]
	reward: 0.0
	terminal: False
	'''
	def processState(self, stateIn):

		# RLAction
		a = RLAction()

		if self.firstAction == True:
			# Get action
			a.action = self.first_action(stateIn.state, stateIn.reward)

			self.info.episode_reward = 0
			self.info.number_actions = 1
		
			# Set firstAction to False to know that it is not firstAction
			self.firstAction = False

			# publish agent's action
			self.outRLAction.publish(a)
		else:			
		    	self.info.episode_reward += stateIn.reward
			self.episodeTimesteps +=1

		    	# If terminal==True, no action to send back, but calculate reward sum
			if stateIn.terminal == True:
				self.last_action(stateIn.state, stateIn.reward)

				# Update model
				self.pgReinforce.updateModel()

				# Add total_rewards of the episode into the history
				self.episodeHistory.append(self.info.episode_reward)

				# Get mean of histories
				meanRewards = np.mean(self.episodeHistory)

				print("Episode: {}; Finished after {} timesteps; Reward for this episode: {}".format(self.info.episode_number, self.episodeTimesteps+1, self.info.episode_reward))
				print("Average reward for last 100 episodes: {}".format(meanRewards))

				if meanRewards >= 195.0 and len(self.episodeHistory) >= 100:
					print("Solved after {} episodes".format(self.info.episode_number+1))

				# Publish episode reward message
				self.outExpInfo.publish(self.info)
		      		self.info.episode_number +=1
		      		self.info.episode_reward = 0
		      		self.firstAction = True
				self.episodeTimesteps = 0
			else:
		      		a.action = self.next_action(stateIn.state, stateIn.reward)
		      		self.info.number_actions +=1

				# publish agent's action
				self.outRLAction.publish(a)

	'''
	this message provides an experience seed for the agent to use for learning.
	'''
	def processSeed(self, seedIn):
		print seedIn
		# print "processSeed"

	'''
	this message describes the environment, number of actions, number of features, if its episodic, etc.

	num_actions: 	7.0
	num_states: 	5.0
	min_state_range: [0.0, 0.0, 0.0, 0.0, 0.0]
	max_state_range: [4.0, 1.0, 3.0, 1.0, 3.0]
	max_reward: 	1.0
	reward_range: 	2.0
	stochastic: False
	episodic: True
	title: Robo
	'''
	def processEnvDescription(self, envIn):
		self.envIn = envIn

		self.pgReinforce = PolicyGradientREINFORCE(self.policyNetwork, self.envIn.num_states, self.envIn.num_actions)

		sr = RLStateReward
		self.info.episode_number = 0
		self.info.episode_reward = 0

		print 'processEnvDescription:'
		print envIn

	def policyNetwork(self, states):
		# Define policy neural network

		# Number of hidden layers = 20
		W1 = tf.get_variable("W1", [self.envIn.num_states, 20], initializer=tf.random_normal_initializer())

		# Set bias (+1 neuron) for the layer
		b1 = tf.get_variable("b1", [20], initializer=tf.constant_initializer(0))

		# Hidden Layer 1
		'''
		Using tanh is like rectified linear unit. It could be useless to put 2 squashing functions of any type back-to-back. But here comes first
		h1 layer, then we create the second output layer p and
		https://www.reddit.com/r/MachineLearning/comments/42lle4/does_it_make_sense_to_use_softmax_right_after/
		'''
		h1 = tf.nn.tanh(tf.matmul(states, W1) + b1)

		W2 = tf.get_variable("W2", [20, self.envIn.num_actions],
			       initializer=tf.random_normal_initializer(stddev=0.1))
		b2 = tf.get_variable("b2", [self.envIn.num_actions],
			       initializer=tf.constant_initializer(0))
		p = tf.matmul(h1, W2) + b2
	  	return p

# Main function.
if __name__ == '__main__':
	# Initialize the node and name it. //, anonymous = True
	rospy.init_node('robo_agent')
	# Go to the main loop.
	
	roboAgentObj = RoboAgent()

	rospy.spin()

