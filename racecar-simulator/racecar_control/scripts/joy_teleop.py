#!/usr/bin/env python

""" JoyTeleop: Control MIT Racecar with Gamepad (Tested on Xbox gamepad)

Get information from the topic /compressed and feed it to the CNN Model.

"""

import rospy

from ackermann_msgs.msg import AckermannDriveStamped
from sensor_msgs.msg import Joy

import sys, select, termios, tty

__version__ = "0.1"
__author__ = "Dmitry Sagoyan"
__maintainer__ = "Dmitry Sagoyan"
__email__ = "s0548717@htw-berlin.de"
__status__ = "Prototype"
__date__ = "5-Feb-2017"


"""
Reading from the Gamepad (XBox) and Publishing to AckermannDriveStamped!
---------------------------
a - full speed      -> will be 1*speedMulti
b - full backward   -> will be -1*speedMulti
x - full stop

axis[0] - left,right ( -1 <-> 1 )
axis[1] - bottom,top ( -1 <-> 1 ) * speedMulti

CTRL-C to quit

"""

speedMulti = 2
speedMulti2 = 1.5

def vels(speed, turn):
    return "Currently:\tSpeed %s\tTurn %s " % (speed,turn)

def joyCallback(data):
    # data.buttons [ 0 = A, 1 = B, 2 = X, 3 = Y ]
    # data.axes[0] left=>negative|right=>positive, 0 center]
    # data.axes[1] bottom=>negative|top=>positive, 0 center]

    msg = AckermannDriveStamped();
    msg.header.stamp = rospy.Time.now();
    msg.header.frame_id = "base_link";

    speed = data.axes[5]
    turn  = data.axes[0]

    if data.buttons[0] == 1:
        speed = 1 * speedMulti
    elif data.buttons[1] == 1:
        speed = -1 * speedMulti
    elif data.buttons[2] == 1:
        speed = 0
        turn  = 0
    else:
        speed = -1* ( (float(speed) - 1.0) * speedMulti2 )

    msg.drive.speed = speed
    msg.drive.acceleration = 0.1
    msg.drive.jerk = 0.1
    msg.drive.steering_angle_velocity = 0

    if turn > -0.1 and turn < 0.1:
        msg.drive.steering_angle = 0
    else: 
        msg.drive.steering_angle = turn

    print(vels(msg.drive.speed, msg.drive.steering_angle))

    pub.publish(msg)


if __name__=="__main__":

    rospy.init_node('joyop')

    joySub = rospy.Subscriber('joy', Joy, joyCallback)
    pub = rospy.Publisher('/vesc/ackermann_cmd_mux/input/teleop', AckermannDriveStamped, queue_size=1)

    # Spin until ctrl + c
    rospy.spin()
